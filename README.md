# CRISPR allele specific effects

Scripts for analysis of sequencing data for Irene Kallimasioti Pazi's PhD thesis.

## Reference genome

Mouse reference genome GRCm38 downloaded from Ensembl FTP server: ftp://ftp.ensembl.org/pub/release-83/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.toplevel.fa.gz

Index with bwa and Bismark.

## Preparing input data

Set up the reads folder with an initial sub-folder ^1_all_reads^ with one sub-folder per sample, named with the sample name. All reads for that sample copied to its folder.

e.g.
```
reads/
  1_all_reads/
    sample1/
      180305_M01270_0437_000000000-BL47N_1_11247WA0069L01_1.fastq.gz
      180305_M01270_0437_000000000-BL47N_1_11247WA0069L01_2.fastq.gz
    sample2/
      180305_M01270_0437_000000000-BL47N_1_11247WA0079L01_1.fastq.gz
      180305_M01270_0437_000000000-BL47N_1_11247WA0079L01_2.fastq.gz
```

List all ids into a file to be used as an input parameter for read processing.

```
cd reads/1_all_reads
ls > ../../params/all.ids.txt
```

Split the ids into genomic vs bisulfite converted sequencing so that you have two files ^params/all.genomic.ids.txt^ and ^params/all.bisulfite.ids.txt^.

Divide the ids again into classes for different processing requirements.

* Class 1 - genomic - split reads by alleles in SNPs defining maternal/paternal origin, then again by evidence for HDR/NHEJ/both/neither
* Class 2 - genomic - only split reads by evidence for HDR/NHEJ/both/neither
* Class 3 - bisulfite - methylated CpG counts only
* Class 4 - bisulfite - split reads by alleles in SNPs defining maternal/paternal origin, then count methylated CpGs

Example parameter files for each of the classes are in the ^params^ folder.

## Prepare reads for alignment and align to reference genome

```
qsub -t 1-<N> src/shell/submit_process_reads.sh reads params/all.ids.txt nextera
qsub -t 1-<G> src/shell/submit_align_sample.sh reads/3_trimmed bams/genomic params/all.genomic.ids.txt
qsub -t 1-<B> src/shell/submit_align_bs_sample.sh reads/3_trimmed bams/bisulfite params/all.bisulfite.ids.txt
```

## Analysis

### Class 1 - genomic

Split reads by alleles in SNPs defining maternal/paternal origin, then again by evidence for HDR/NHEJ/both/neither, and tabulate the results.

```
qsub -t 1-6 src/shell/submit_split_reads_by_snps.sh bams/genomic/class1 params/coords.genomic.class1.txt --output
qsub -t 1-6 src/shell/submit_classify_reads.sh bams/genomic/class1 params/coords.genomic.class1.txt
perl src/perl/tabulate_standard_counts.pl --input bams/genomic/class1 --output counts/genomic.class1.counts.txt
```

### Class 2 - genomic

Split reads by evidence for HDR/NHEJ/both/neither, and tabulate the results.

```
qsub -t 1-2 src/shell/submit_classify_reads_no_snps.sh bams/genomic/class2 params/coords.genomic.class2.txt
perl src/perl/tabulate_standard_counts_no_snp.pl --input bams/genomic/class2 --output counts/genomic.class2.counts.txt
```

### Class 3 - bisulfite

Run methylation counting and tabulate the results.

```
qsub -t 1-13 src/shell/submit_methylation_counts.sh bams/bisulfite/class3 params/coords.bisulfite.class3.txt

cd bams/bisulfite/class3

R
res = data.frame()
for (file in list.files(pattern = "methylation.counts.out"))
{
  s = strsplit(file, ".", fixed=TRUE)

  x = read.table(file, header=T)
  x$total = x$methylated + x$unmethylated
  x$methylated_prop = x$methylated / x$total
  y = subset(x, x$conflicting == 0)

  y$count = 1
  y$status = "mixed"
  y$status[y$methylated_prop <= 0.2] = "unmethylated"
  y$status[y$methylated_prop >= 0.8] = "methylated"
  y$status = as.factor(y$status)

  z = aggregate(y$count, by = list(status = y$status), sum)
  z$sample = s[[1]][1]

  res = rbind(res, z)
}
res$sample = as.factor(res$sample)
write.table(res, "../../../counts/class3.methylation.count.summary.txt", sep="\t", col.names=TRUE, row.names=FALSE, quote=FALSE)
```

### Class 4 - bisulfite

Split reads by alleles in SNPs defining maternal/paternal origin, then count methylated CpGs.

```
qsub -t 1-7 ../src/shell/submit_split_reads_by_snps.sh ../bams/bisulfite/class4 ../params/coords.bisulfite.class4.txt --output
qsub -t 1-7 ../src/shell/submit_methylation_counts.sh ../bams/bisulfite/class4 ../params/coords.bisulfite.class4.txt

cd bams/bisulfite/class4

R
res = data.frame()
for (file in c(list.files(pattern = "ref.methylation.counts.out"), list.files(pattern = "alt.methylation.counts.out")))
{
  s = strsplit(file, ".", fixed=TRUE)

  x = read.table(file, header=T)
  x$total = x$methylated + x$unmethylated
  x$methylated_prop = x$methylated / x$total
  y = subset(x, x$conflicting == 0)

  y$count = 1
  y$status = "mixed"
  y$status[y$methylated_prop <= 0.2] = "unmethylated"
  y$status[y$methylated_prop >= 0.8] = "methylated"
  y$status = as.factor(y$status)

  z = aggregate(y$count, by = list(status = y$status), sum)
  z$sample = s[[1]][1]
  z$alleles = s[[1]][2]

  res = rbind(res, z)
}
res$sample = as.factor(res$sample)
res$alleles = as.factor(res$alleles)
write.table(res, "../../../counts/class4.methylation.count.summary.txt", sep="\t", col.names=TRUE, row.names=FALSE, quote=FALSE)
```

### Indel types and sizes for NHEJ samples

For each entry in the coords file for a class, e.g. this one with 5 lines, get all the indels, their size and leftmost coordinate.

```
for ((i = 1; i <= 5; i = i + 1))
do
  src/shell/run_all_indels.sh bams/genomic/class1 params/coords.genomic.class1.txt $i
done
```

Determine the 5 most common indels by allele in maternal/paternal determining SNP for each replicate group, defined by sample name prefix.

```
prefix=<sample_prefix>
for allele in ref alt
do
  perl src/perl/most_common_indels_in_nhej_reads.pl --input <dir_with_indel_counts> --prefix $prefix --allele $allele | sort -k4 -nr | head -n 5 > $prefix.$allele.NHEJ.most.common.indels.txt
done
```

Length distributions of indels by replicate group, time point, strain, and allele.

```
prefix=<sample_prefix>
time=<time_point>
strain=<J or B>
for allele in ref alt
do
  perl src/perl/indel_lengths_in_nhej_reads.pl --input <dir_with_indel_size_and_coord_files> --prefix ${prefix}_${time}_${strain} --allele $allele > ${prefix}_${time}_${strain}.$allele.NHEJ.indel_length_dist.txt
done
```

Indel count classes - where i is the ith line in the parameter file.

```
../../src/shell/run_indel_length_dist.sh . ../../params/indel_classification.class2.txt $i
```
