#!/usr/bin/perl -w

=head1 NAME

most_common_indels_in_nhej_reads.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Reads in the NHEJ.all.indels.coords.out files for a set of 3 replicates.
Excludes reads with multiple indels. Determines the 5 most common indels
across the set of replicates and the proportion of reads with those
indels weighted by number of NHEJ reads in each replicate.

=cut

use strict;

# Perl
use IO::File;
use Getopt::Long;

my $usage = qq{USAGE:
$0 [--help]
  --input  Input directory
  --prefix Prefix of files (ends in 1,2,3 then \$allele.NHEJ.all.indels.coords.out)
  --allele ref or alt
  --nhej_start NHEJ region start coordinate
  --neh_end    NHEJ region end coordinate
};

my $help = 0;
my $input_dir;
my $prefix;
my $allele;
my $nhej_start;
my $nhej_end;

GetOptions(
    'help'        => \$help,
    'input=s'     => \$input_dir,
    'prefix=s'    => \$prefix,
    'allele=s'    => \$allele,
    'nhej_start=i' => \$nhej_start,
    'nhej_end=i'   => \$nhej_end
) or die $usage;

if ($help || !$input_dir || !$prefix || !$allele || !$nhej_start || !$nhej_end)
{
    print $usage;
    exit(0);
}

my %indels;
my %nhej_reads_per_sample = ( 1 => 0, 2 => 0, 3 => 0 );
for (my $i = 1; $i <= 3; $i++)
{
    my $input_file = "$input_dir/$prefix$i.$allele.NHEJ.all.indels.coords.out";
    if ($prefix eq 'Meg3_JB' && $i == 1)
    {
	$input_file = sprintf "$input_dir/$prefix%d_rpt.$allele.NHEJ.all.indels.coords.out", $i;
    }

    my $in_fh = new IO::File;
    $in_fh->open("$input_file", "r") or die "Could not open $input_file\n$!";

    my %reads;
    while (my $line = <$in_fh>)
    {
	chomp $line;
	my ($read, $indel_type, $length, $start, $end) = split("\t", $line);

	if (($start >= $nhej_start && $start <= $nhej_end) ||
	    ($end >= $nhej_start && $end <= $nhej_end))
	{
	    $reads{$read}{$indel_type}{"$start-$end:$length"}++;
	}
    }

    $nhej_reads_per_sample{$i} = scalar(keys %reads);

    foreach my $read (keys %reads)
    {
	my $deletion_count = exists($reads{$read}{'D'}) ? scalar(keys %{ $reads{$read}{'D'} }) : 0;
	my $insertion_count = exists($reads{$read}{'I'}) ? scalar(keys %{ $reads{$read}{'I'} }) : 0;

	if ($deletion_count >= 1 && $insertion_count >= 1)
	{
	    my $deletions = join(",", sort keys %{ $reads{$read}{'D'} });
	    my $insertions = join(",", sort keys %{ $reads{$read}{'I'} });

	    $indels{'DI'}{"$deletions\t$insertions"}{$i}++;
	    $indels{'DI'}{"$deletions\t$insertions"}{'total'}++;
	}
	elsif ($deletion_count >= 1 && $insertion_count == 0)
	{
	    my $deletions = join(",", sort keys %{ $reads{$read}{'D'} });

	    $indels{'D'}{"$deletions\t."}{$i}++;
	    $indels{'D'}{"$deletions\t."}{'total'}++;
	}
	elsif ($insertion_count >= 1 && $deletion_count == 0)
	{
	    my $insertions = join(",", sort keys %{ $reads{$read}{'I'} });

	    $indels{'I'}{".\t$insertions"}{$i}++;
	    $indels{'I'}{".\t$insertions"}{'total'}++;
	}
    }
}

my $total_nhej_reads = $nhej_reads_per_sample{1} + $nhej_reads_per_sample{2} + $nhej_reads_per_sample{3};
foreach my $indel_type ('D', 'I', 'DI')
{
    foreach my $coords (keys %{ $indels{$indel_type} })
    {
       	my $weighted_prop = 0;
	foreach my $replicate (1, 2, 3)
	{
	    if (exists($indels{$indel_type}{$coords}{$replicate}))
	    {
		$weighted_prop += $indels{$indel_type}{$coords}{$replicate} / $total_nhej_reads;
	    }
	}

	printf STDOUT "$indel_type\t$coords\t%0.8f\t%d\n", $weighted_prop, $indels{$indel_type}{$coords}{'total'};
    }
}
