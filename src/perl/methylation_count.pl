#!/usr/bin/perl -w

=head1 NAME

methylation_count.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Counts number of methylated CpGs in each read of a BAM file.

=cut

use strict;

# BioPerl
use Bio::DB::Sam;

# Perl
use IO::File;
use Getopt::Long;

my $usage = qq{USAGE:
$0 [--help]
  --bam        Input BAM file
  --output     Output file name
  --seqname    Name of sequence (INPP5f, KCNQ)
  --start      Start coordinate of region to check
  --end        End coordinate of region to check
  --nhej_start Start of NHEJ region
  --nhej_end   End of NHEJ region
};

my $help = 0;
my $bam_file;
my $output_file;
my $seq_name;
my $start;
my $end;
my $nhej_start = -1;
my $nhej_end = -1;

GetOptions(
	   'help'        => \$help,
	   'bam=s'       => \$bam_file,
	   'output=s'    => \$output_file,
	   'seqname=s'   => \$seq_name,
	   'start=i'     => \$start,
	   'end=i'       => \$end,
           'nhej_start=i' => \$nhej_start,
           'nhej_end=i'   => \$nhej_end
) or die $usage;

if ($help || !$bam_file || !$output_file || !$seq_name || !$start || !$end)
{
    print $usage;
    exit(0);
}

# open the BAM file
my $sam = Bio::DB::Sam->new(-bam => $bam_file);

my @alns = $sam->get_features_by_location(-type=>'match', -seq_id=>$seq_name, -start=>$start, -end=>$end);

my %reads;
for (my $i = 0; $i < scalar(@alns); $i++)
{
    my @cigar = @{ $alns[$i]->cigar_array };

    my $full_read_name = $alns[$i]->query->name;
    $full_read_name =~ /(.+)\_[12]/;
    my $read_name = $1;
    
    my $aln_start = $alns[$i]->start;
    my $aln_end = $alns[$i]->end;
    my $pos = $aln_start;

    my $meth_line = $alns[$i]->get_tag_values('XM');
    my @meth_seq = split(//, $meth_line);
    my $meth_idx = 0;

    for (my $j = 0; $j < scalar(@cigar); $j++)
    {
	# First check the cigar type
	my $cigar_type = $cigar[$j]->[0];
	my $cigar_count = $cigar[$j]->[1];
	
	if ($cigar_type eq 'M')
	{
	    # matched sequence, check CpG positions and increment genomic pos and meth index accordingly
	    for (my $k = 0; $k < $cigar_count; $k++)
	    {
		if ($nhej_start < 0 || ($nhej_start > 0 && ($pos < $nhej_start || $pos > $nhej_end)))
		{
		    # Check if this is a CpG position with methylation status
		    if ($meth_seq[$meth_idx] eq 'Z')
		    {
			$reads{$read_name}{$pos}{'methylated'}++;
		    }
		    elsif ($meth_seq[$meth_idx] eq 'z')
		    {
			$reads{$read_name}{$pos}{'unmethylated'}++;
		    }
		}
		$pos++;
		$meth_idx++;
	    }
	}
	elsif ($cigar_type eq 'I')
	{
	    # genomic pos is unchanged
	    # methylation index increases
	    $meth_idx += $cigar_count;
	}
	elsif ($cigar_type eq 'D')
	{
	    # genomic pos increases
	    $pos += $cigar_count;
	    # methylation index increases
	}
    }
}

my $out_fh = new IO::File;
$out_fh->open("$output_file", "w") or die "Could not open $output_file\n$!";

print $out_fh "read\tmethylated\tunmethylated\tconflicting\n";

foreach my $read (keys %reads)
{
    my @positions = sort { $a <=> $b } keys %{ $reads{$read} };
    my $methylated_count = 0;
    my $unmethylated_count = 0;
    my $conflicting_count = 0;
    for (my $i = 0; $i < scalar(@positions); $i++)
    {
	next if ($positions[$i] < $start || $positions[$i] > $end);

	if (exists($reads{$read}{$positions[$i]}{'methylated'}) && !exists($reads{$read}{$positions[$i]}{'unmethylated'}))
	{
	    $methylated_count++;
	}
	elsif (!exists($reads{$read}{$positions[$i]}{'methylated'}) && exists($reads{$read}{$positions[$i]}{'unmethylated'}))
	{
	    $unmethylated_count++;
	}
	elsif (exists($reads{$read}{$positions[$i]}{'methylated'}) && exists($reads{$read}{$positions[$i]}{'unmethylated'}))
	{
	    $conflicting_count++;
	}
    }
    
    printf $out_fh "$read\t$methylated_count\t$unmethylated_count\t$conflicting_count\n";
}

$out_fh->close();
