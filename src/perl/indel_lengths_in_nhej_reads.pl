#!/usr/bin/perl -w

=head1 NAME

indel_lengths_in_nhej_reads.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Reads in the NHEJ.all.indels.coords.out files for a set of 3 replicates.
Excludes reads with multiple indels. Counts the number of reads with 
insertions and deletions of each length for each replicate.

=cut

use strict;

# Perl
use IO::File;
use Getopt::Long;

my $usage = qq{USAGE:
$0 [--help]
  --input  Input directory
  --prefix Prefix of files (ends in 1,2,3 then \$allele.NHEJ.all.indels.coords.out)
  --allele ref or alt
  --nhej_start NHEJ region start coordinate
  --nhej_end   NHEJ region end coordinate
};

my $help = 0;
my $input_dir;
my $prefix;
my $allele;
my $nhej_start;
my $nhej_end;

GetOptions(
    'help'        => \$help,
    'input=s'     => \$input_dir,
    'prefix=s'    => \$prefix,
    'allele=s'    => \$allele,
    'nhej_start=i' => \$nhej_start,
    'nhej_end=i'   => \$nhej_end
) or die $usage;

if ($help || !$input_dir || !$prefix || !$allele || !$nhej_start || !$nhej_end)
{
    print $usage;
    exit(0);
}

my %indels;
my %nhej_reads_per_sample;
for (my $i = 1; $i <= 3; $i++)
{
    my $input_file = "$input_dir/$prefix$i.$allele.NHEJ.all.indels.coords.out";
    if ($prefix eq 'Meg3_JB' && $i == 1)
    {
	$input_file = sprintf "$input_dir/$prefix%d_rpt.$allele.NHEJ.all.indels.coords.out", $i;
    }

    my $in_fh = new IO::File;
    $in_fh->open("$input_file", "r") or die "Could not open $input_file\n$!";

    my %reads;
    while (my $line = <$in_fh>)
    {
	chomp $line;
	my ($read, $indel_type, $length, $start, $end) = split("\t", $line);

	if (($start >= $nhej_start && $start <= $nhej_end) ||
	    ($end >= $nhej_start && $end <= $nhej_end))
	{
	    $reads{$read}{$indel_type}{"$start-$end:$length"}++;
	}
    }

    $nhej_reads_per_sample{$i} = scalar(keys %reads);

    foreach my $read (keys %reads)
    {
	my $deletion_count = exists($reads{$read}{'D'}) ? scalar(keys %{ $reads{$read}{'D'} }) : 0;
	my $insertion_count = exists($reads{$read}{'I'}) ? scalar(keys %{ $reads{$read}{'I'} }) : 0;

	if ($deletion_count == 1 && $insertion_count == 0)
	{
	    my $deletions = join(",", sort keys %{ $reads{$read}{'D'} });
	    my ($coords, $length) = split(/:/, $deletions);

	    $indels{'D'}{$length}{$i}++;
	    $indels{'D'}{$length}{'total'}++;
	}
	elsif ($insertion_count == 1 && $deletion_count == 0)
	{
	    my $insertions = join(",", sort keys %{ $reads{$read}{'I'} });
	    my ($coords, $length) = split(/:/, $insertions);

	    $indels{'I'}{$length}{$i}++;
	    $indels{'I'}{$length}{'total'}++;
	}
    }
}

my $total_nhej_reads = $nhej_reads_per_sample{1} + $nhej_reads_per_sample{2} + $nhej_reads_per_sample{3};

print STDOUT "indel\tlength\trep1.prop\trep1.count\trep2.prop\trep2.count\trep3.prop\trep3.count\tweighted.avg\total\n";
foreach my $indel_type ('D', 'I')
{
    foreach my $length (sort { $a <=> $b } keys %{ $indels{$indel_type} })
    {
	print STDOUT "$indel_type\t$length";
	my $weighted_proportion = 0;
	foreach my $replicate (1, 2, 3)
	{
	    if (exists($indels{$indel_type}{$length}{$replicate}))
	    {
		printf STDOUT "\t%0.8f\t%d", $indels{$indel_type}{$length}{$replicate} / $nhej_reads_per_sample{$replicate}, $indels{$indel_type}{$length}{$replicate};
		$weighted_proportion += $indels{$indel_type}{$length}{$replicate} / $total_nhej_reads;
	    }
	    else
	    {
		printf STDOUT "\t%0.8f\t0", 0;
	    }
	}
	printf STDOUT "\t%0.8f\t%d\n", $weighted_proportion, $indels{$indel_type}{$length}{'total'};
    }
}
