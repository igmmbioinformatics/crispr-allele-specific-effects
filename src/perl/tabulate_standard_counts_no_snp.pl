#!/usr/bin/perl -w

=head1 NAME

tabulate_standard_counts_no_snp.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Reads in the classified.out files from the usual split reads by
HDR/NHEJ evidence without taking into account split by SNP
and produces a table for import into Excel.

=cut

use strict;

# Perl
use IO::File;
use Getopt::Long;

my $usage = qq{USAGE:
$0 [--help]
  --input  Input directory
  --output Output file name
};

my $help = 0;
my $input_dir;
my $output_file;

GetOptions(
	   'help'        => \$help,
	   'input=s'     => \$input_dir,
	   'output=s'    => \$output_file
) or die $usage;

if ($help || !$input_dir || !$output_file)
{
    print $usage;
    exit(0);
}

opendir(DIR, $input_dir);
my @classified_out_files = grep(/classified\.out$/, readdir(DIR));
closedir(DIR);

my %counts;
my @samples;
foreach my $file (@classified_out_files)
{
    my $in_fh = new IO::File;
    $in_fh->open("$input_dir/$file", "r") or die "Could not open $input_dir/$file\n$!";

    my $sample;
    while (my $line = <$in_fh>)
    {
	chomp $line;
	if ($line =~ /(.+)\.bam$/)
	{
	    $sample = $1;
	    push(@samples, $sample);
	}
	else
	{
	    my ($read_type, $count) = split(/\s/, $line);
	    if ($read_type eq 'total:')
	    {
		$read_type = "total";
	    }
	    $counts{$sample}{$read_type} = $count;
	}
    }
}

my $out_fh = new IO::File;
$out_fh->open("$output_file", "w") or die "Could not open $output_file\n$!";

print $out_fh "sample\ttotal\tHDR\tNHEJ\tboth\tneither\n";

foreach my $sample (@samples)
{
    print $out_fh "$sample";
    
    foreach my $read_type ('total', 'HDR', 'NHEJ', 'both', 'neither')
    {
	printf $out_fh "\t%d", $counts{$sample}{$read_type};
    }

    print $out_fh "\n";
}

$out_fh->close();
