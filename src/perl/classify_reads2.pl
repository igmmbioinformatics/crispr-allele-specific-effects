#!/usr/bin/perl -w

=head1 NAME

classify_reads.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Classifies reads according to containing HDR fragment or NHEJ evidence.

=cut

use strict;

# BioPerl
use Bio::DB::Sam;

# Perl
use IO::File;
use Getopt::Long;

my $usage = qq{USAGE:
$0 [--help]
  --bam       Input BAM file
  --reference Path to reference sequence
  --output    Output files (default don't)
  --seqname   Name of sequence (INPP5f, KCNQ)
  --hdr       Format pos:allele,pos2:allele2 etc.
  --nhej      Start-end coordinates to check for indels (NHEJ evidence)
  --start     Start coordinate of region to check
  --end       End coordinate of region to check
};

my $help = 0;
my $bam_file;
my $reference_file;
my $output = 0;
my $seq_name;
my $hdr_snp_line;
my $nhej_pos_line;
my $start;
my $end;

GetOptions(
	   'help'        => \$help,
	   'bam=s'       => \$bam_file,
	   'reference=s' => \$reference_file,
	   'output'      => \$output,
	   'seqname=s'   => \$seq_name,
	   'hdr=s'       => \$hdr_snp_line,
	   'nhej=s'      => \$nhej_pos_line,
	   'start=i'     => \$start,
	   'end=i'       => \$end
) or die $usage;

if ($help || !$bam_file || !$reference_file || !$seq_name || !$hdr_snp_line || !$nhej_pos_line || !$start || !$end)
{
    print $usage;
    exit(0);
}

my @snp_info = split(',', $hdr_snp_line);
my %snps;
foreach my $si (@snp_info)
{
    my ($pos, $allele) = split(':', $si);
    $snps{$pos} = $allele;
}

my ($nhej_start, $nhej_end) = split('-', $nhej_pos_line);

# open the BAM file
my $sam = Bio::DB::Sam->new(-bam => $bam_file, -fasta=> $reference_file);

my @alns = $sam->get_features_by_location(-type=>'match', -seq_id=>$seq_name, -start=>$start, -end=>$end);

my %counts;
my %reads;
for (my $i = 0; $i < scalar(@alns); $i++)
{
    my @cigar = @{ $alns[$i]->cigar_array };
    my ($ref, $matches, $query) = $alns[$i]->padded_alignment;
    
    my $aln_start = $alns[$i]->start;
    my $aln_end = $alns[$i]->end;

    my @ref_seq = split(//, $ref);
    my @query_seq = split(//, $query);
    my $pos = $aln_start;
    my %snp_check;
    my $is_nhej = 0;
    my $cigar_index = 0;
    my $cigar_type = $cigar[$cigar_index]->[0];
    my $cigar_count = $cigar[$cigar_index]->[1];
    my $cigar_pos = 0;

    $reads{$alns[$i]->query->name}{'exists'}++;

    for (my $j = 0; $j < scalar(@ref_seq); $j++)
    {
	# if this is one of the HDR SNP positions to check, get the query base and compare it to the HDR allele
	if (exists($snps{$pos}))
	{
	    if ($query_seq[$j] eq $snps{$pos})
	    {
		$reads{$alns[$i]->query->name}{'snps'}{$pos}++;
	    }
	}

	# if this is one of the NHEJ indel positions to check, test for indel
	if ($pos >= $nhej_start && $pos <= $nhej_end && ($query_seq[$j] eq "-" || $ref_seq[$j] eq "-") && ($cigar_type eq "D" || $cigar_type eq "I"))
	{
	    $reads{$alns[$i]->query->name}{'nhej'}++;
	}

	# update the genomic position
	if ($ref_seq[$j] eq "-")
	{
	    # insertion, leave position alone
	}
	else
	{
	    # either deletion, match, or substitution, increment by one
	    $pos++;
	}
	
	# update the position in the cigar string array
	$cigar_pos++;
	if ($cigar_pos == $cigar_count)
	{
	    $cigar_index++;
	    if ($cigar_index < scalar(@cigar))
	    {
		$cigar_pos = 0;
		$cigar_type = $cigar[$cigar_index]->[0];
		$cigar_count = $cigar[$cigar_index]->[1];
	    }
	}
    }
}

my %map;
my $hdr_snp_count = scalar(keys %snps);
foreach my $read_name (keys %reads)
{
    my $is_nhej = 0;
    if (exists($reads{$read_name}{'nhej'}))
    {
	$is_nhej = 1;
    }

    my $is_hdr = 0;
    if (exists($reads{$read_name}{'snps'}))
    {
	my $count = scalar(keys %{ $reads{$read_name}{'snps'} });
	if ($count == $hdr_snp_count)
	{
	    $is_hdr = 1;
	}
    }

    if ($is_nhej && !$is_hdr)
    {
	$counts{'NHEJ'}++;
	$map{$read_name} = "NHEJ";
    }
    elsif (!$is_nhej && $is_hdr)
    {
	$counts{'HDR'}++;
	$map{$read_name} = "HDR";
    }
    elsif (!$is_nhej && !$is_hdr)
    {
	$counts{'neither'}++;
	$map{$read_name} = "neither";
    }
    else
    {
	$counts{'both'}++;
	$map{$read_name} = "both";
    }
}

printf "total: %d\n", scalar(keys %reads);
my @types = ('HDR', 'NHEJ', 'neither', 'both');
foreach my $type (@types)
{
    if (exists($counts{$type}))
    {
	printf "$type\t%d\n", $counts{$type};
    }
    else
    {
	printf "$type\t0\n";
    }
}

if ($output)
{
    my $bname=`basename $bam_file`;
    chomp $bname;
    $bname =~ /(.+)\.bam/;
    my $name = $1;
    
    my $in_bam = Bio::DB::Bam->open($bam_file, "r");
    my $header = $in_bam->header();
    
    my %out_bams;
    foreach my $type (@types)
    {
	my $out_bam = Bio::DB::Bam->open("$name.$type.bam", "w");
	$out_bam->header_write($header);
	
	$out_bams{$type} = $out_bam;
    }
    
    while (my $aln = $in_bam->read1())
    {
	my $read_name = $aln->qname;
	next if (!exists($map{$read_name}));
	$out_bams{$map{$read_name}}->write1($aln);
    }
}
