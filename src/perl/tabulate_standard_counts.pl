#!/usr/bin/perl -w

=head1 NAME

tabulate_standard_counts.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Reads in the snp.counts and classified.out files from the usual split reads by
SNP alleles and then by HDR/NHEJ evidence and produces a table for import into
Excel.

=cut

use strict;

# Perl
use IO::File;
use Getopt::Long;

my $usage = qq{USAGE:
$0 [--help]
  --input  Input directory
  --output Output file name
};

my $help = 0;
my $input_dir;
my $output_file;

GetOptions(
	   'help'        => \$help,
	   'input=s'     => \$input_dir,
	   'output=s'    => \$output_file
) or die $usage;

if ($help || !$input_dir || !$output_file)
{
    print $usage;
    exit(0);
}

opendir(DIR, $input_dir);
my @snp_count_files = grep(/snp\.counts$/, readdir(DIR));
closedir(DIR);
opendir(DIR, $input_dir);
my @classified_out_files = grep(/classified\.out$/, readdir(DIR));
closedir(DIR);

my %counts;
my @samples;
foreach my $file (@snp_count_files)
{
    my $in_fh = new IO::File;
    $in_fh->open("$input_dir/$file", "r") or die "Could not open $input_dir/$file\n$!";

    my $sample;
    while (my $line = <$in_fh>)
    {
	chomp $line;
	if ($line =~ /(.+)\.bam$/)
	{
	    $sample = $1;
	    push(@samples, $sample);
	}
	else
	{
	    my ($snp_allele, $count) = split(/: /, $line);
	    $counts{$sample}{$snp_allele}{'total'} = $count;
	}
    }
}

foreach my $file (@classified_out_files)
{
    my $in_fh = new IO::File;
    $in_fh->open("$input_dir/$file", "r") or die "Could not open $input_dir/$file\n$!";

    my $sample;
    my $snp_allele;
    while (my $line = <$in_fh>)
    {
	chomp $line;
	if ($line =~ /(.+)\.(ref|alt)\.bam$/)
	{
	    $sample = $1;
	    $snp_allele = $2;
	}
	else
	{
	    my ($read_type, $count) = split(/\t/, $line);
	    $counts{$sample}{$snp_allele}{$read_type} = $count;
	}
    }
}

my $out_fh = new IO::File;
$out_fh->open("$output_file", "w") or die "Could not open $output_file\n$!";

print $out_fh "sample\tref.total\tref.HDR\tref.NHEJ\tref.both\tref.neither\talt.total\talt.HDR\talt.NHEJ\talt.both\talt.neither\n";

foreach my $sample (@samples)
{
    print $out_fh "$sample";
    foreach my $snp_allele ('ref', 'alt')
    {
	my $total_check = 0;
	foreach my $read_type ('HDR', 'NHEJ', 'both', 'neither')
	{
	    $total_check += $counts{$sample}{$snp_allele}{$read_type};
	}
	if ($total_check != $counts{$sample}{$snp_allele}{'total'})
	{
	    print STDERR "$sample $snp_allele counts don't add up\n";
	}

	foreach my $read_type ('total', 'HDR', 'NHEJ', 'both', 'neither')
	{
	    printf $out_fh "\t%d", $counts{$sample}{$snp_allele}{$read_type};
	}
    }
    print $out_fh "\n";
}

$out_fh->close();
