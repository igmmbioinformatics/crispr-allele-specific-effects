#!/usr/bin/perl -w

=head1 NAME

nhej_indel_size_and_first_coord.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Processes indels.out files to get just the first coordinate of the indel for a given read.

=cut

use strict;

# Perl
use IO::File;
use Getopt::Long;

my $usage = qq{USAGE:
$0 [--help]
  --input     indels.out file
  --output    Output file name
};

my $help = 0;
my $input_file;
my $output_file;

GetOptions(
	   'help'        => \$help,
	   'input=s'     => \$input_file,
	   'output=s'    => \$output_file,
) or die $usage;

if ($help || !$input_file || !$output_file)
{
    print $usage;
    exit(0);
}

my $in_fh = new IO::File;
my $out_fh = new IO::File;
$in_fh->open($input_file, "r") or die "Could not open $input_file\n$!";
$out_fh->open("$output_file", "w") or die "Could not open $output_file\n$!";

my $current_read;
my $current_indel;
my $current_length;
my $current_first_coord = -1;
my $current_last_coord;
while (my $line = <$in_fh>)
{
    chomp $line;
    if ($current_first_coord < 0)
    {
	($current_read, $current_indel, $current_length, $current_first_coord) = split(/\t/, $line);
	$current_last_coord = $current_first_coord;
	next;
    }

    my ($next_read, $next_indel, $next_length, $next_coord) = split(/\t/, $line);

    if ($current_read eq $next_read && $current_indel eq $next_indel && $current_length == $next_length && $current_last_coord + 1 == $next_coord)
    {
	$current_last_coord = $next_coord;
    }
    else
    {
	print $out_fh "$current_read\t$current_indel\t$current_length\t$current_first_coord\t$current_last_coord\n";
	$current_read = $next_read;
	$current_indel = $next_indel;
	$current_length = $next_length;
	$current_first_coord = $next_coord;
	$current_last_coord = $next_coord;
    }
}

print $out_fh "$current_read\t$current_indel\t$current_length\t$current_first_coord\t$current_last_coord\n";

$in_fh->close();
$out_fh->close();
