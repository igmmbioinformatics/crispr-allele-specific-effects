#!/usr/bin/perl -w

=head1 NAME

indel_count_classes.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Reads in the NHEJ.all.indels.coords.out files for a set of 3 replicates.
Outputs the number of reads with different combinations of indel counts.

=cut

use strict;

# Perl
use IO::File;
use Getopt::Long;

my $usage = qq{USAGE:
$0 [--help]
  --input  Input directory
  --prefix Prefix of files (ends in 1,2,3 then \$allele.NHEJ.all.indels.coords.out)
  --allele ref or alt
  --nhej_start start coordinate of NHEJ region (-10 from cleavage site)
  --nhej_end   end coordinate of NHEJ region (+10 from cleavage site)
};

my $help = 0;
my $input_dir;
my $prefix;
my $allele;
my $nhej_start;
my $nhej_end;

GetOptions(
    'help'        => \$help,
    'input=s'     => \$input_dir,
    'prefix=s'    => \$prefix,
    'allele=s'    => \$allele,
    'nhej_start=i' => \$nhej_start,
    'nhej_end=i'   => \$nhej_end
) or die $usage;

if ($help || !$input_dir || !$prefix || !$allele || !$nhej_start || !$nhej_end)
{
    print $usage;
    exit(0);
}

my %indels;
my %nhej_reads_per_sample = ( 1 => 0, 2 => 0, 3 => 0 );
my %reads_used_per_sample = ( 1 => 0, 2 => 0, 3 => 0 );
my %indel_counts_per_read;
for (my $i = 1; $i <= 3; $i++)
{
    my $input_file = "$input_dir/$prefix$i.$allele.NHEJ.all.indels.coords.out";

    my $in_fh = new IO::File;
    $in_fh->open("$input_file", "r") or die "Could not open $input_file\n$!";

    my %reads;
    while (my $line = <$in_fh>)
    {
	chomp $line;
	my ($read, $indel_type, $length, $start, $end) = split("\t", $line);

	if (($start >= $nhej_start && $start <= $nhej_end) ||
	    ($end >= $nhej_start && $end <= $nhej_end))
	{
	    $reads{$read}{$indel_type}{"$length:$start:$end"}++;
	}
    }

    $nhej_reads_per_sample{$i} = scalar(keys %reads);

    foreach my $read (keys %reads)
    {
	my $deletion_count = exists($reads{$read}{'D'}) ? scalar(keys %{ $reads{$read}{'D'} }) : 0;
	my $insertion_count = exists($reads{$read}{'I'}) ? scalar(keys %{ $reads{$read}{'I'} }) : 0;

	$indel_counts_per_read{"D:$deletion_count\tI:$insertion_count"}++;
    }
}

foreach my $count_class (keys %indel_counts_per_read)
{
    printf STDOUT "$count_class\t%d\n", $indel_counts_per_read{$count_class};
}
