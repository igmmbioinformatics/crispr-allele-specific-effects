#!/usr/bin/perl -w

=head1 NAME

split_reads_by_snps.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Classifies reads according to containing SNPs or not

=cut

use strict;

# BioPerl
use Bio::DB::Sam;

# Perl
use IO::File;
use Getopt::Long;

my $usage = qq{USAGE:
$0 [--help]
  --bam         Input BAM file
  --reference   Path to reference sequence
  --seqname     Name of sequence
  --snps        Format pos:ref/alt,pos2:ref/alt etc.
  --start       Start coordinate of region to check
  --end         End coordinate of region to check
  --output      Output reads by SNP status (default don't do this)
};

my $help = 0;
my $bam_file;
my $reference_file;
my $seq_name;
my $snp_line;
my $start;
my $end;
my $output = 0;

GetOptions(
	   'help'        => \$help,
	   'bam=s'       => \$bam_file,
	   'reference=s' => \$reference_file,
	   'seqname=s'   => \$seq_name,
	   'snps=s'      => \$snp_line,
	   'start=i'     => \$start,
	   'end=i'       => \$end,
           'output'      => \$output
) or die $usage;

if ($help || !$bam_file || !$reference_file || !$seq_name || !$snp_line || !$start || !$end)
{
    print $usage;
    exit(0);
}

my @snp_info = split(',', $snp_line);
my %snps;
foreach my $si (@snp_info)
{
    my ($pos, $ref, $alt) = split(/[\:\/]/, $si);
    $snps{$pos}{'ref'} = $ref;
    $snps{$pos}{'alt'} = $alt;
}

# open the BAM file
my $sam = Bio::DB::Sam->new(-bam => $bam_file, -fasta => $reference_file, -autoindex => 1);

my @alns = $sam->get_features_by_location(-type=>'match', -seq_id=>$seq_name, -start=>$start, -end=>$end);

my %reads;
for (my $i = 0; $i < scalar(@alns); $i++)
{
    my ($ref, $matches, $query) = $alns[$i]->padded_alignment;
    
    my $aln_start = $alns[$i]->start;
    my $aln_end = $alns[$i]->end;

    my @ref_seq = split(//, $ref);
    my @query_seq = split(//, $query);
    my $pos = $aln_start;
    my %ref_alleles;
    my %alt_alleles;
    for (my $j = 0; $j < scalar(@ref_seq); $j++)
    {
	# if this is one of the SNP positions to check, compare to the ref and alt alleles
	if (exists($snps{$pos}))
	{
	    if ($query_seq[$j] eq $snps{$pos}{'ref'})
	    {
		$reads{$alns[$i]->query->name}{'ref'}{$pos}++;
	    }
	    elsif ($query_seq[$j] eq $snps{$pos}{'alt'})
	    {
		$reads{$alns[$i]->query->name}{'alt'}{$pos}++;
	    }
	}

	# update the position
	if ($ref_seq[$j] eq "-")
	{
	    # insertion, leave position alone
	}
	else
	{
	    # either deletion, match, or substitution, increment by one
	    $pos++;
	}
    }
}

my %counts;
my %map;
my $snp_count = scalar(keys %snps);
foreach my $read (keys %reads)
{
    my $ref_count = exists($reads{$read}{'ref'}) ? scalar(keys %{ $reads{$read}{'ref'} }) : 0;
    my $alt_count = exists($reads{$read}{'alt'}) ? scalar(keys %{ $reads{$read}{'alt'} }) : 0;

    if ($ref_count == $snp_count && $alt_count == 0)
    {
	$counts{'ref'}++;
	$map{$read} = 'ref';
    }
    elsif ($ref_count == 0 && $alt_count == $snp_count)
    {
	$counts{'alt'}++;
	$map{$read} = 'alt';
    }
    else
    {
	$counts{'na'}++;
    }
}

printf "ref: %d\n", exists($counts{'ref'}) ? $counts{'ref'} : 0;
printf "alt: %d\n", exists($counts{'alt'}) ? $counts{'alt'} : 0;
printf "na: %d\n",  exists($counts{'na'}) ? $counts{'na'} : 0;

if ($output)
{
    my $bname=`basename $bam_file`;
    chomp $bname;
    $bname =~ /(.+)\.bam/;
    my $name = $1;

    my $in_bam = Bio::DB::Bam->open($bam_file, "r");
    my $header = $in_bam->header();

    my %out_bams;
    my @types = ('ref', 'alt');
    foreach my $type (@types)
    {
	my $out_bam = Bio::DB::Bam->open("$name.$type.bam", "w");
	$out_bam->header_write($header);
	
	$out_bams{$type} = $out_bam;
    }

    while (my $aln = $in_bam->read1())
    {
	my $read_name = $aln->qname;
	next if (!exists($map{$read_name}));
	$out_bams{$map{$read_name}}->write1($aln);
    }
}
