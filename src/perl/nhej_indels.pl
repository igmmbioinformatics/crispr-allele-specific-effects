#!/usr/bin/perl -w

=head1 NAME

nhej_indels.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Outputs indels in reads for NHEJ BAM files.

=cut

use strict;

# BioPerl
use Bio::DB::Sam;

# Perl
use IO::File;
use Getopt::Long;

my $usage = qq{USAGE:
$0 [--help]
  --bam       Input BAM file
  --reference Path to reference sequence
  --output    Output file name
  --seqname   Name of sequence (INPP5f, KCNQ)
  --start     Start coordinate of region to check
  --end       End coordinate of region to check
};

my $help = 0;
my $bam_file;
my $reference_file;
my $output_file;
my $seq_name;
my $start;
my $end;

GetOptions(
	   'help'        => \$help,
	   'bam=s'       => \$bam_file,
	   'reference=s' => \$reference_file,
	   'output=s'    => \$output_file,
	   'seqname=s'   => \$seq_name,
	   'start=i'     => \$start,
	   'end=i'       => \$end
) or die $usage;

if ($help || !$bam_file || !$reference_file || !$output_file || !$seq_name || !$start || !$end)
{
    print $usage;
    exit(0);
}

# open the BAM file
my $sam = Bio::DB::Sam->new(-bam => $bam_file, -fasta=> $reference_file);

my @alns = $sam->get_features_by_location(-type=>'match', -seq_id=>$seq_name, -start=>$start, -end=>$end);

my $out_fh = new IO::File;
$out_fh->open("$output_file", "w") or die "Could not open $output_file\n$!";
my $read_count = 0;
my %indels;
for (my $i = 0; $i < scalar(@alns); $i++)
{
    my @cigar = @{ $alns[$i]->cigar_array };
    my ($ref, $matches, $query) = $alns[$i]->padded_alignment;
    
    my $aln_start = $alns[$i]->start;
    my $aln_end = $alns[$i]->end;

    my @ref_seq = split(//, $ref);
    my @query_seq = split(//, $query);
    my $pos = $aln_start;
    my $cigar_index = 0;
    my $cigar_type = $cigar[$cigar_index]->[0];
    my $cigar_count = $cigar[$cigar_index]->[1];
    my $cigar_pos = 0;

    for (my $j = 0; $j < scalar(@ref_seq); $j++)
    {
	# test for indel
	if (($query_seq[$j] eq "-" || $ref_seq[$j] eq "-") && ($cigar_type eq "D" || $cigar_type eq "I"))
	{
	    printf $out_fh "%s\t$cigar_type\t$cigar_count\t$pos\n", $alns[$i]->query->name;
	}

	# update the genomic position
	if ($ref_seq[$j] eq "-")
	{
	    # insertion, leave position alone
	}
	else
	{
	    # either deletion, match, or substitution, increment by one
	    $pos++;
	}
	
	# update the position in the cigar string array
	$cigar_pos++;
	if ($cigar_pos == $cigar_count)
	{
	    $cigar_index++;
	    if ($cigar_index < scalar(@cigar))
	    {
		$cigar_pos = 0;
		$cigar_type = $cigar[$cigar_index]->[0];
		$cigar_count = $cigar[$cigar_index]->[1];
	    }
	}
    }
}

$out_fh->close();
