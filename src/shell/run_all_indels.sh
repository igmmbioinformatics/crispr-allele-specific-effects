#!/bin/bash 
#$ -N nhej_indels
#$ -j y 
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=1:00:00
#$ -l h_vmem=4G

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/perl/5.24.0
reference=/exports/igmm/datastore/NextGenResources/reference/GRCm38/Mus_musculus.GRCm38.dna.toplevel.fasta
src=/exports/igmm/eddie/bioinfsvice/ameynert/awood-lab/Nov2017/analysis/src/perl

BAMDIR=$1
PARAMS=$2
INDEX=$3

PREFIX=`head -n $INDEX $PARAMS | tail -n 1 | awk '{ print $1 }'`
SEQNAME=`head -n $INDEX $PARAMS | tail -n 1 | awk '{ print $2 }'`
LOCUS_START=`head -n $INDEX $PARAMS | tail -n 1 | awk '{ print $3 }'`
LOCUS_END=`head -n $INDEX $PARAMS | tail -n 1 | awk '{ print $4 }'`

cd $BAMDIR

for name in $PREFIX*.NHEJ.bam
do
  samtools index $name
  perl $src/nhej_indels.pl --bam $name --reference $reference --output ${name%.bam}.all.indels.out --seqname $SEQNAME --start $LOCUS_START --end $LOCUS_END
  perl $src/nhej_indel_size_and_first_coord.pl --input ${name%.bam}.all.indels.out --output ${name%.bam}.all.indels.coords.out

  cut -f 4 ${name%.bam}.all.indels.coords.out | sort -n | uniq -c > ${name%.bam}.all.indels.start.coord.counts.txt
  cut -f 5 ${name%.bam}.all.indels.coords.out | sort -n | uniq -c > ${name%.bam}.all.indels.end.coord.counts.txt
done
