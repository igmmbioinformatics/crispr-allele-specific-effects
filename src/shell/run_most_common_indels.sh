#!/bin/bash 
#$ -N nhej_indels
#$ -j y 
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=1:00:00
#$ -l h_vmem=4G

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/perl/5.24.0
src=/exports/igmm/eddie/bioinfsvice/ameynert/awood-lab/Nov2017/analysis/src/perl

BAMDIR=$1
PARAMS=$2
INDEX=$3

PREFIX=`head -n $INDEX $PARAMS | tail -n 1 | awk '{ print $1 }'`
NHEJ_START=`head -n $INDEX $PARAMS | tail -n 1 | awk '{ print $2 }'`
NHEJ_END=`head -n $INDEX $PARAMS | tail -n 1 | awk '{ print $3 }'`

cd $BAMDIR

for allele in ref alt
do
  perl $src/most_common_indels_in_nhej_reads.pl --prefix $PREFIX --allele $allele --nhej_start $NHEJ_START --nhej_end $NHEJ_END --input . | sort -k5 -nr | head -n 5 > $PREFIX.$allele.NHEJ.most.common.indels.txt
done
