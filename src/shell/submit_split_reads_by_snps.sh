# To run this script, do 
# qsub -t 1-n submit_split_reads_by_snps.sh BAMDIR PARAMS OUTPUT_READS
# where n is the number of lines in params/ids.txt

#$ -N split_reads_by_snps
#$ -j y 
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=02:00:00
#$ -l h_vmem=2G

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/perl/5.24.0
module load igmm/apps/samtools/1.2

perldir=/exports/igmm/eddie/bioinfsvice/ameynert/awood_genome_editing/analysis/src/perl
reference=/exports/igmm/datastore/NextGenResources/reference/GRCm38/Mus_musculus.GRCm38.dna.toplevel.fasta

# Classify reads within different categories as ref or alt alleles. 

BAMDIR=$1
PARAMS=$2
OUTPUT_READS=$3

PREFIX=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $1 }'`
SEQNAME=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $2 }'`
LOCUS_START=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $3 }'`
LOCUS_END=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $4 }'`
SNPS=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $5 }'`

cd $BAMDIR

rm $PREFIX.snp.counts
for name in $PREFIX*.bam
do
  echo $name >> $PREFIX.snp.counts
  perl $perldir/split_reads_by_snps.pl --bam $name --reference $reference --seqname $SEQNAME --snps $SNPS --start $LOCUS_START --end $LOCUS_END $OUTPUT_READS >> $PREFIX.snp.counts
done
