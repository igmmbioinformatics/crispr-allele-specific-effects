# To run this script, do 
# qsub -t 1-n submit_classify_reads.sh BAMDIR PARAMS OUTPUT_READS
# where n is the number of lines in params/ids.txt

#$ -N classify_reads
#$ -j y 
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=02:00:00
#$ -l h_vmem=2G

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/perl/5.24.0
module load igmm/apps/samtools/1.2

perldir=/exports/igmm/eddie/bioinfsvice/ameynert/awood_genome_editing/analysis/src/perl
reference=/exports/igmm/datastore/NextGenResources/reference/GRCm38/Mus_musculus.GRCm38.dna.toplevel.fasta

# Classify all reads as indicating HDR (as indicated by the fact that they have the template SNVs, as indicating NHEJ (they have an indel within the region of interest), or having evidence of both or neither.  

BAMDIR=$1
PARAMS=$2
OUTPUT=$3

PREFIX=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $1 }'`
SEQNAME=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $2 }'`
LOCUS_START=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $3 }'`
LOCUS_END=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $4 }'`
HDR=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $6 }'`
NHEJ_START=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $7 }'`
NHEJ_END=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $8 }'`

cd $BAMDIR

rm $PREFIX.classified.out
for name in $PREFIX*ref.bam $PREFIX*alt.bam
do 
  echo $name >> $PREFIX.classified.out
  samtools index $name
  perl $perldir/classify_reads2.pl --bam $name --reference $reference --seqname $SEQNAME --hdr $HDR --nhej $NHEJ_START-$NHEJ_END --start $LOCUS_START --end $LOCUS_END $OUTPUT >> $PREFIX.classified.out
done
