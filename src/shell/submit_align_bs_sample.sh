# To run this script, do 
# qsub -t 1-n submit_align_sample.sh INDIR OUTDIR SAMPLES
# where n is the number of lines in names.list 

#$ -N align_bs
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=2:00:00
#$ -l h_vmem=15G

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/bismark/0.16.3
module load igmm/libs/ncurses/6.0
module load igmm/libs/htslib/1.4 
module load igmm/apps/samtools/1.4
module load igmm/apps/bowtie/2.2.6

ref=/exports/igmm/eddie/NextGenResources/bismark_genome_indexes/mm10_lambda_phage/

INDIR=$1
OUTDIR=$2
SAMPLES=$3

NAME=`head -n $SGE_TASK_ID $SAMPLES | tail -n 1`

mkdir -p $OUTDIR/$NAME

READS1=$INDIR/$NAME/$NAME.reads_1.fastq.gz 
READS2=$INDIR/$NAME/$NAME.reads_2.fastq.gz

BASE1=$NAME.reads_1.fastq.gz
BASE2=$NAME.reads_2.fastq.gz

UNMAPPED=$OUTDIR/$NAME/$NAME.unmapped

bismark -N 1 --rdg 2,1 --rfg 2,1 -o $OUTDIR/$NAME --genome $ref -1 $READS1 -2 $READS2 --unmapped $UNMAPPED

cd $OUTDIR/$NAME

READS1_UNMAPPED=$NAME.reads_1.fastq.gz_unmapped_reads_1.fq.gz 
READS2_UNMAPPED=$NAME.reads_2.fastq.gz_unmapped_reads_2.fq.gz

bismark -N 1 --rdg 2,1 --rfg 2,1 --genome $ref $READS1_UNMAPPED
bismark -N 1 --rdg 2,1 --rfg 2,1 --pbat --genome $ref $READS2_UNMAPPED

BISMARK_PE_BAM=$NAME.reads_1_bismark_bt2_pe.bam
BISMARK_SE_1_BAM=$NAME.reads_1.fastq.gz_unmapped_reads_1_bismark_bt2.bam
BISMARK_SE_2_BAM=$NAME.reads_2.fastq.gz_unmapped_reads_2_bismark_bt2.bam

samtools merge $NAME.merged.bam $BISMARK_PE_BAM $BISMARK_SE_1_BAM $BISMARK_SE_2_BAM
samtools sort $NAME.merged.bam > $NAME.bam
samtools index $NAME.bam

rm $NAME.merged.bam $BISMARK_PE_BAM $BISMARK_SE_1_BAM $BISMARK_SE_2_BAM
rm *.fq.gz

