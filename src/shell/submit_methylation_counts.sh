# To run this script, do 
# qsub -t 1-n submit_methylation_counts.sh BAMDIR PARAMS
# where n is the number of lines in params/coords.txt

#$ -N methylation_counts
#$ -j y 
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=00:30:00
#$ -l h_vmem=1G

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/perl/5.24.0
module load igmm/apps/samtools/1.2

perldir=/exports/igmm/eddie/bioinfsvice/ameynert/awood_genome_editing/analysis/src/perl
reference=/exports/igmm/datastore/NextGenResources/reference/GRCm38/Mus_musculus.GRCm38.dna.toplevel.fasta

BAMDIR=$1
PARAMS=$2

PREFIX=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $1 }'`
SEQNAME=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $2 }'`
LOCUS_START=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $3 }'`
LOCUS_END=`head -n $SGE_TASK_ID $PARAMS | tail -n 1 | awk '{ print $4 }'`

cd $BAMDIR

for name in $PREFIX*bam
do 
  if [ ! -f $name.bai ]
  then
    samtools index $name
  fi

  if [ ! -f ${name%.bam}.methylation.counts.out ]
  then
    perl $perldir/methylation_count.pl --bam $name --output ${name%.bam}.methylation.counts.out --seqname $SEQNAME --start $LOCUS_START --end $LOCUS_END
  fi

done
