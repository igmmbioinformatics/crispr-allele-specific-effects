# To run this script, do 
# qsub -t 1-n submit_align_sample.sh INDIR OUTDIR PARAMS
# where n is the number of lines in params/ids.txt

#$ -N align
#$ -j y 
#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=01:00:00
#$ -l h_vmem=16G

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/bwa/0.7.12-r1039
module load igmm/libs/ncurses/6.0
module load igmm/libs/htslib/1.4 
module load igmm/apps/samtools/1.4

ref=/exports/igmm/eddie/NextGenResources/reference/GRCm38/Mus_musculus.GRCm38.dna.toplevel.fasta

INDIR=$1
OUTDIR=$2
SAMPLES=$3

NAME=`head -n $SGE_TASK_ID $SAMPLES | tail -n 1`

READS1=$INDIR/$NAME/$NAME.reads_1.fastq.gz 
READS2=$INDIR/$NAME/$NAME.reads_2.fastq.gz 

mkdir -p $OUTDIR

bwa mem -M $ref $READS1 $READS2 | samtools view -Su /dev/stdin | samtools sort /dev/stdin > $OUTDIR/$NAME.bam

samtools index $OUTDIR/$NAME.bam
