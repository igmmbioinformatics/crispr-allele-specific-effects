# To run this script, do 
#  qsub -t 1-n submit_process_reads.sh INDIR SAMPLES ADAPTOR
# where n in the number of lines in params/ids.txt

#$ -N process_reads
#$ -j y 
#$ -cwd 
#$ -l h_rt=1:00:00
#$ -l h_vmem=8G

. /etc/profile.d/modules.sh
MODULEPATH=$MODULEPATH:/exports/igmm/software/etc/el7/modules

resources=/exports/igmm/eddie/NextGenResources
module load igmm/apps/FastQC
module load igmm/apps/bcbio
module load igmm/apps/TrimGalore

INDIR=$1
PARAMS=$2
ADAPTOR=$3

NAME=`head -n $SGE_TASK_ID $PARAMS | tail -n 1`

cd $INDIR

# 1. FastQC
fastqc --no-extract -o 1_all_reads/$NAME 1_all_reads/$NAME/*.gz

gunzip 1_all_reads/$NAME/*.gz

# 2. De-duplicate reads
mkdir -p 2_deduplicated/$NAME

ls 1_all_reads/$NAME/*fastq > 1_all_reads/$NAME/fastuniq.input.txt

$resources/software/FastUniq_1.1/source/fastuniq -i 1_all_reads/$NAME/fastuniq.input.txt -t q -o 2_deduplicated/$NAME/$NAME.reads_1.fastq -p 2_deduplicated/$NAME/$NAME.reads_2.fastq

gzip -f 1_all_reads/$NAME/*.fastq 2_deduplicated/$NAME/*.fastq

rm 1_all_reads/$NAME/fastuniq.input.txt

# 3. FastQC on de-duplicated reads
fastqc --no-extract -o 2_deduplicated/$NAME 2_deduplicated/$NAME/*.fastq.gz

# 4. Adaptor and quality trimming
mkdir -p 3_trimmed/$NAME

trim_galore --$ADAPTOR --retain_unpaired --paired --stringency 5 --output_dir 3_trimmed/$NAME 2_deduplicated/$NAME/$NAME.reads_1.fastq.gz 2_deduplicated/$NAME/$NAME.reads_2.fastq.gz

cd 3_trimmed/$NAME

mv $NAME.reads_1_val_1.fq.gz $NAME.reads_1.fastq.gz
mv $NAME.reads_2_val_2.fq.gz $NAME.reads_2.fastq.gz

# 5. FASTQC on adaptor and quality-trimmed reads
fastqc --no-extract *.fastq.gz
